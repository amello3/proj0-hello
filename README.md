# Proj0-Hello
-------------

#Author:
--------
-Austin Mello

#Contact Information
--------------------
- Email: amello3@uoregon.edu

- Phone: 530-276-1662

- Zoom: amello3@uoregon.edu

#Description:
-------------
- Prints "Hello world". (Nothing more and nothing less)
